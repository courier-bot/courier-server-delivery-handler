# frozen_string_literal: true

module CourierHandlers
  # Default handler that will be used during tests.
  class TestHandler < CourierHandlers::BaseHandler
    def deliver(_fields)
      CourierHandlers::DeliveryResponse.new(
        true,
        'external_id'
      )
    end

    def self.callback(_args)
      CourierHandlers::CallbackRequest.new(
        true,
        -1,
        nil
      )
    end
  end
end
