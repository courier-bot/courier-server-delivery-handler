# frozen_string_literal: true

RSpec.describe AdapterService, type: :service do
  # rubocop:disable RSpec/LeakyConstantDeclaration
  # Test handler for method expectations.
  class AdapterServiceMockHandler < CourierHandlers::BaseHandler
    def deliver(_fields)
      :delivery_response
    end

    def callback(_args)
      :callback_request
    end
  end
  # rubocop:enable RSpec/LeakyConstantDeclaration

  let(:instance) { described_class.new(adapter) }

  let(:adapter) do
    {
      'id' => '54a12e8e-d66c-4391-93a7-178190f82465',
      'code' => 'adapter_code',
      'uri' => AdapterServiceMockHandler.name,
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => nil
    }
  end

  describe '.initialize' do
    subject(:instance) { described_class.new(adapter) }

    let(:adapter) do
      {
        'id' => '51ea5985-cd8e-4450-9623-c855d8020adc',
        'code' => 'adapter_code',
        'uri' => handler_type,
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => nil
      }
    end

    let(:handler_type) { AdapterServiceMockHandler.name }

    it 'sets the `handler` instance variable' do
      instance

      expect(instance.instance_variable_get(:@handler)).to be_a(AdapterServiceMockHandler)
    end

    context 'when the handler type is not found' do
      let(:handler_type) { 'UnknownClass' }

      it 'raises an `AdapterUnknownError` error' do
        expect { instance }.to raise_error(
          AdapterUnknownError,
          'Adapter could not be found: `UnknownClass`.'
        )
      end
    end
  end

  describe '#deliver' do
    subject(:service) { instance.deliver(fields) }

    let(:fields) { instance_double(Hash) }

    it 'forwards the client info to the handler' do
      expect(service).to eq(:delivery_response)
    end
  end

  describe '#callback' do
    subject(:service) { instance.callback(params) }

    let(:params) { instance_double(Hash) }

    it 'forwards the params to the handler' do
      expect(service).to eq(:callback_request)
    end
  end
end
