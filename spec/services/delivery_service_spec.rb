# frozen_string_literal: true

require 'support/test_handler'

RSpec.describe DeliveryService, type: :service do
  before do
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_message)
      .with('8e4e881f-a759-4195-9ca1-112ff17d6b88').and_return(message)

    allow(internal_api_service).to receive(:retrieve_message_kind)
      .with('fad35fef-4545-4549-8b39-2556345b1a62').and_return(message_kind)

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('a5c3b0c4-8b20-4789-8269-1bb9c5d604fe').and_return(message_kind_adapter)

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('38fbd897-ecd4-4526-943c-a6547f913de0').and_return(delivery_kind_adapter)

    allow(internal_api_service).to receive(:retrieve_delivery_method_by_code)
      .with('delivery_method').and_return(delivery_method)

    allow(internal_api_service).to receive(:retrieve_delivery_kind_by_delivery_method)
      .with('922a9c2b-7bf1-4425-907f-631bf8cbfe13').and_return(delivery_kind)

    allow(internal_api_service).to receive(:update_message) do |_message_id, attrs|
      message.merge(attrs)
    end

    allow(internal_api_service).to receive(:create_delivery)
      .with(hash_including('messageId' => '8e4e881f-a759-4195-9ca1-112ff17d6b88')) do |attrs|
        delivery.merge('message_id' => attrs['messageId']).merge(attrs)
      end

    allow(internal_api_service).to receive(:create_delivery_field)
      .with(hash_including('deliveryId' => '6799ba3d-915d-4564-8ede-04efa06026d5')) do |attrs|
        delivery_field.merge('delivery_id' => attrs['deliveryId']).merge(attrs)
      end

    allow(internal_api_service).to receive(:update_delivery)
      .with('6799ba3d-915d-4564-8ede-04efa06026d5', anything) do |_delivery_id, attrs|
        delivery.merge(attrs)
      end
  end

  let(:instance) { described_class.new }

  let(:internal_api_service) { instance_double(InternalApiService) }

  let(:message_kind_adapter) do
    {
      'id' => 'a5c3b0c4-8b20-4789-8269-1bb9c5d604fe',
      'code' => 'adapter_code',
      'uri' => 'http://example.com/path',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_kind_adapter) do
    {
      'id' => '38fbd897-ecd4-4526-943c-a6547f913de0',
      'code' => 'adapter_code',
      'uri' => 'CourierHandlers::TestHandler',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => nil
    }
  end

  let(:message_kind) do
    {
      'id' => 'fad35fef-4545-4549-8b39-2556345b1a62',
      'code' => 'message_kind_code',
      'adapterId' => 'a5c3b0c4-8b20-4789-8269-1bb9c5d604fe',
      'createdAt' => '2000-01-02T03:04:07Z',
      'updatedAt' => '2000-01-02T03:04:08Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_kind) do
    {
      'id' => '59b7b3a3-f2a9-41b8-93d6-edde2f4b1cc7',
      'code' => 'delivery_kind_code',
      'deliveryMethodId' => '922a9c2b-7bf1-4425-907f-631bf8cbfe13',
      'adapterId' => '38fbd897-ecd4-4526-943c-a6547f913de0',
      'createdAt' => '2000-01-02T03:04:09Z',
      'updatedAt' => '2000-01-02T03:04:10Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_method) do
    {
      'id' => '922a9c2b-7bf1-4425-907f-631bf8cbfe13',
      'code' => 'delivery_kind_code',
      'createdAt' => '2000-01-02T03:04:11Z',
      'updatedAt' => '2000-01-02T03:04:12Z',
      'deletedAt' => nil
    }
  end

  # The `'pending'` string has a unary `plus` sign to unfreeze it. This is to prevent the `mutant`
  # gem from failing its `eql?`/`equal?`` tests, while keeping the magic comment about string
  # literals at the beginning of the file for the `rubocop` gem.
  let(:message) do
    {
      'id' => '8e4e881f-a759-4195-9ca1-112ff17d6b88',
      'messageKindId' => 'fad35fef-4545-4549-8b39-2556345b1a62',
      'targetMessageable' => 'targetMessageable',
      'status' => +'pending',
      'createdAt' => '2000-01-02T03:04:11Z',
      'updatedAt' => '2000-01-02T03:04:12Z'
    }
  end

  let(:delivery) do
    {
      'id' => '6799ba3d-915d-4564-8ede-04efa06026d5',
      'messageId' => '8e4e881f-a759-4195-9ca1-112ff17d6b88',
      'deliveryKindId' => '59b7b3a3-f2a9-41b8-93d6-edde2f4b1cc7',
      'externalId' => 'external_id',
      'status' => 'pending',
      'undeliveredReason' => nil,
      'createdAt' => '2000-01-02T03:04:13Z',
      'updatedAt' => '2000-01-02T03:04:14Z'
    }
  end

  let(:delivery_field) do
    {
      'id' => 'c68979e9-31fa-427d-8f50-7701515209e1',
      'deliveryId' => '6799ba3d-915d-4564-8ede-04efa06026d5',
      'name' => 'name',
      'value' => 'value',
      'createdAt' => '2000-01-02T03:04:15Z',
      'updatedAt' => '2000-01-02T03:04:16Z'
    }
  end

  describe '#deliver' do
    subject(:service) { instance.deliver(specified_delivery_fields) }

    before do
      stub_request(:post, 'example.com/path')
        .with(headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' },
              body: client_request_body.to_json)
        .to_return(client_response)

      allow(AdapterService).to receive(:new).with(delivery_kind_adapter).and_return(adapter_service)

      allow(adapter_service).to receive(:deliver).and_return(delivery_response)
    end

    let(:specified_delivery_fields) do
      { 'messageId' => '8e4e881f-a759-4195-9ca1-112ff17d6b88' }
    end

    let(:client_request_body) do
      {
        'id' => '8e4e881f-a759-4195-9ca1-112ff17d6b88',
        'messageKindId' => 'fad35fef-4545-4549-8b39-2556345b1a62',
        'targetMessageable' => 'targetMessageable',
        'status' => 'delivering',
        'createdAt' => '2000-01-02T03:04:11Z',
        'updatedAt' => '2000-01-02T03:04:12Z'
      }
    end

    let(:client_response) do
      {
        body: client_response_body.to_json,
        status: 200
      }
    end

    let(:client_response_body) do
      {
        'deliveryMethod' => 'delivery_method',
        'to' => 'to_user',
        'content' => 'content'
      }
    end

    let(:adapter_service) { instance_spy(AdapterService.name) }

    let(:delivery_response) do
      CourierHandlers::DeliveryResponse.new(
        true,
        'external_id'
      )
    end

    it 'updates the message status to `delivering`' do
      service

      expect(internal_api_service).to have_received(:update_message)
        .with('8e4e881f-a759-4195-9ca1-112ff17d6b88',
              'status' => 'delivering')
    end

    it 'creates a new delivery' do
      service

      expect(internal_api_service).to have_received(:create_delivery)
        .with('messageId' => '8e4e881f-a759-4195-9ca1-112ff17d6b88',
              'deliveryKindId' => '59b7b3a3-f2a9-41b8-93d6-edde2f4b1cc7',
              'status' => 'delivering')
    end

    it 'creates a new delivery field for each of the client information' do
      service

      client_response_body.each do |key, value|
        expect(internal_api_service).to have_received(:create_delivery_field)
          .with('deliveryId' => '6799ba3d-915d-4564-8ede-04efa06026d5',
                'name' => key,
                'value' => value)
      end
    end

    it 'delivers the message using the courier handler' do
      service

      expect(adapter_service).to have_received(:deliver)
        .with(client_response_body)
    end

    it 'updates the delivery status to `unconfirmed`' do
      service

      expect(internal_api_service).to have_received(:update_delivery)
        .with('6799ba3d-915d-4564-8ede-04efa06026d5',
              'externalId' => 'external_id',
              'status' => 'unconfirmed')
    end

    it 'updates the message status to `delivered`' do
      service

      expect(internal_api_service).to have_received(:update_message)
        .with('8e4e881f-a759-4195-9ca1-112ff17d6b88',
              'status' => 'delivered')
    end

    context 'when the message is not set to `pending`' do
      before { message['status'] = 'delivering' }

      it 'does not create a new delivery' do
        service

        expect(internal_api_service).not_to have_received(:create_delivery)
      end
    end

    context 'when the delivery is cancelled by the client' do
      let(:client_response) do
        {
          body: '',
          status: 204
        }
      end

      it 'updates the message status to `undelivered`' do
        service

        expect(internal_api_service).to have_received(:update_message)
          .with('8e4e881f-a759-4195-9ca1-112ff17d6b88',
                'status' => 'undelivered',
                'undelivered_reason' => 'Cancelled by client.')
      end

      it 'does not create a new delivery' do
        service

        expect(internal_api_service).not_to have_received(:create_delivery)
      end

      it 'does not deliver anything' do
        service

        expect(adapter_service).not_to have_received(:deliver)
      end
    end

    context 'when the delivery is a failure' do
      let(:delivery_response) do
        CourierHandlers::DeliveryResponse.new(
          false,
          'external_id'
        )
      end

      it 'updates the delivery status to `undelivered`' do
        service

        expect(internal_api_service).to have_received(:update_delivery)
          .with('6799ba3d-915d-4564-8ede-04efa06026d5',
                'externalId' => 'external_id',
                'status' => 'undelivered',
                'undeliveredReason' => 'Delivery failure.')
      end

      it 'updates the message status to `pending`' do
        service

        expect(internal_api_service).to have_received(:update_message)
          .with('8e4e881f-a759-4195-9ca1-112ff17d6b88',
                'status' => 'pending')
      end
    end
  end
end
