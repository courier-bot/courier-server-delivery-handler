# frozen_string_literal: true

# Service to deliver messages.
class DeliveryService
  # Delivers the specified message.
  #
  # This will first request information from the client, then deliver the message by means of the
  # configured adapter.
  #
  # @param message [Hash] Message to deliver.
  def deliver(delivery_fields)
    message = internal_api_service.retrieve_message(delivery_fields.fetch('messageId'))

    message_id = message.fetch('id')
    Rails.logger.info "Delivering message id `#{message_id}`"

    # Message has an invalid status, possibly because it has since been modified. Skip it.
    return unless message.fetch('status').eql?('pending')

    message = internal_api_service.update_message(message_id, 'status' => 'delivering')

    client_info = request_client_info(message)

    if client_info.nil?
      internal_api_service.update_message(
        message_id,
        'status' => 'undelivered',
        'undelivered_reason' => 'Cancelled by client.'
      )

      return
    end

    delivery_kind = delivery_kind_from_delivery_method_code(client_info.fetch('deliveryMethod'))
    delivery = create_delivery(message, delivery_kind, client_info)

    deliver_message(delivery, delivery_kind, client_info)
  end

  private

  def internal_api_service
    @internal_api_service ||= InternalApiService.new
  end

  def request_client_info(message)
    message_id = message.fetch('id')

    message_kind_id = message.fetch('messageKindId')
    message_kind = internal_api_service.retrieve_message_kind(message_kind_id)

    adapter_id = message_kind.fetch('adapterId')
    adapter = internal_api_service.retrieve_adapter(adapter_id)

    adapter_code = adapter.fetch('code')
    adapter_uri = adapter.fetch('uri')
    Rails.logger.info "Requesting message id `#{message_id}`'s information from client linked by " \
      "adapter `#{adapter_code}` (URI `#{adapter_uri}`)"

    response = Faraday.post(
      adapter_uri,
      message.to_json,
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    )

    Rails.logger.info "Client `#{adapter_code}` responded with status `#{response.status}` and " \
      "client info `#{response.body}`"

    # Client wants to stop delivering the message.
    return if response.status.equal?(204)

    JSON.parse(response.body)
  end

  def delivery_kind_from_delivery_method_code(delivery_method_code)
    delivery_method = internal_api_service.retrieve_delivery_method_by_code(delivery_method_code)
    internal_api_service.retrieve_delivery_kind_by_delivery_method(delivery_method.fetch('id'))
  end

  def create_delivery(message, delivery_kind, fields)
    delivery = internal_api_service.create_delivery(
      'messageId' => message.fetch('id'),
      'deliveryKindId' => delivery_kind.fetch('id'),
      'status' => 'delivering'
    )

    fields.each do |key, value|
      internal_api_service.create_delivery_field(
        'deliveryId' => delivery.fetch('id'),
        'name' => key,
        'value' => value
      )
    end

    delivery
  end

  def deliver_message(delivery, delivery_kind, fields)
    delivery_id = delivery.fetch('id')
    delivery_kind_code = delivery_kind.fetch('code')
    Rails.logger.info "Delivering delivery id `#{delivery_id}` via delivery kind " \
      "`#{delivery_kind_code}`"

    adapter_id = delivery_kind.fetch('adapterId')
    adapter = internal_api_service.retrieve_adapter(adapter_id)

    adapter_service = AdapterService.new(adapter)
    delivery_response = adapter_service.deliver(fields)

    if delivery_response.success
      Rails.logger.info "Delivery id `#{delivery_id}` delivered successfully to third-party service"

      # The delivery is set to `'unconfirmed'` because the server only knows that the third-party
      # service has accepted to deliver the message. As far as it knows, the delivery was
      # successful, but the server will only be certain when/if it receives a confirmation during
      # a callback request.

      internal_api_service.update_delivery(
        delivery_id,
        'externalId' => delivery_response.external_id,
        'status' => 'unconfirmed'
      )

      internal_api_service.update_message(delivery.fetch('messageId'), 'status' => 'delivered')
    else
      Rails.logger.info "Delivery id `#{delivery_id}` failed to deliver by third-party service"

      # The request to the third-party service has failed, or the third-party service has refused
      # to deliver the message. The message is set to `pending` to be picked up by the Message
      # Queuer on its next pass.

      internal_api_service.update_delivery(
        delivery_id,
        'externalId' => delivery_response.external_id,
        'status' => 'undelivered',
        'undeliveredReason' => 'Delivery failure.'
      )

      internal_api_service.update_message(delivery.fetch('messageId'), 'status' => 'pending')
    end
  end
end
