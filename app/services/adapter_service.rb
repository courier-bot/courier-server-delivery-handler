# frozen_string_literal: true

# Service related to adapters. Access to adapters should be limited to methods in this service.
class AdapterService
  # The adapter class.
  attr_reader :handler

  # Initializes a new adapter service for the relevant `CourierHandler::BaseHandler` class.
  #
  # @param adapter [Adapter] The adapter object that points to the correct handler.
  # @raise [AdapterUnknownError] If the adapter's name does not point to a valid handler, which
  #   was not found in the descendant classes of `CourierHandlers::BaseHandler`. In development
  #   environments, this might also mean that the handler is not yet loaded. This would normally be
  #   done in an initializer when configuring the handlers.
  def initialize(adapter)
    # TODO: Change this to `adapter.fetch('name')` when the `name` attribute is added.
    handler_type = adapter.fetch('uri')
    @handler = retrieve_handler_class(handler_type).new
  end

  # Makes a delivery request to the handler.
  #
  # @param fields [Hash] The necessary fields for the third-party service to perform the delivery.
  # @return [CourierHandlers::DeliveryResponse] The normalized response.
  delegate :deliver, to: :handler

  # Forwards a third-party service's callback to the handler for processing.
  #
  # The return value can also be an instance of `CourierHandlers::CallbackIgnoreRequest`, which
  # indicates that the server should ignore the callback. This is typically used for when the server
  # receives callbacks about events that it doesn't care about.
  #
  # @param params [Hash] The third-party service's request body.
  # @return [CourierHandlers::CallbackRequest] The normalized request.
  delegate :callback, to: :handler

  private

  def retrieve_handler_class(handler_type)
    CourierHandlers::BaseHandler.descendants.find do |handler|
      handler.name.eql?(handler_type)
    end || unknown_adapter(handler_type)
  end

  def unknown_adapter(handler_type)
    raise AdapterUnknownError,
          "Adapter could not be found: `#{handler_type}`."
  end
end
