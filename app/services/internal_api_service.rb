# frozen_string_literal: true

# Service to connect with the Courier Server's Internal API.
class InternalApiService
  attr_reader :base_uri

  # Initializes a new instance of a service to communicate with the Internal API.
  def initialize
    host = ENV['INTERNAL_API_HOST']
    port = ENV['INTERNAL_API_PORT']

    @base_uri = "http://#{host}:#{port}"
  end

  # Retrieves the message with the specified id.
  #
  # @param message_id [String] Id of the message.
  def retrieve_message(message_id)
    response = perform_get_request("/messages/#{message_id}")

    JSON.parse(response.body)
  end

  # Updates a message with the specified attributes.
  #
  # @param message_id [String] Id of the message.
  # @param attrs [Hash] Attributes to set.
  def update_message(message_id, attrs)
    response = perform_patch_request(
      "/messages/#{message_id}",
      attrs.to_json
    )

    JSON.parse(response.body)
  end

  # Creates a new delivery with the specified attributes.
  #
  # @param attrs [Hash] Attributes to set.
  def create_delivery(attrs)
    response = perform_post_request(
      '/deliveries',
      attrs.to_json
    )

    JSON.parse(response.body)
  end

  # Updates a delivery with the specified attributes.
  #
  # @param delivery_id [String] Id of the delivery.
  # @param attrs [Hash] Attributes to set.
  def update_delivery(delivery_id, attrs)
    response = perform_patch_request(
      "/deliveries/#{delivery_id}",
      attrs.to_json
    )

    JSON.parse(response.body)
  end

  # Creates a new delivery field with the specified attributes.
  #
  # @param attrs [Hash] Attributes to set.
  def create_delivery_field(attrs)
    response = perform_post_request(
      '/deliveryFields',
      attrs.to_json
    )

    JSON.parse(response.body)
  end

  # Retrieves the message kind with the specified id.
  #
  # @param message_kind_id [String] Id of the message kind.
  def retrieve_message_kind(message_kind_id)
    response = perform_get_request("/messageKinds/#{message_kind_id}")

    JSON.parse(response.body)
  end

  # Retrieves the adapter with the specified id.
  #
  # @param adapter_id [String] Id of the adapter.
  def retrieve_adapter(adapter_id)
    response = perform_get_request("/adapters/#{adapter_id}")

    JSON.parse(response.body)
  end

  # Retrieves the delivery kind with the specified delivery method. There should only be one valid
  # delivery kind for a given delivery method, so this will blindly take the first one.
  #
  # @param delivery_method_id [String] Delivery method id of the delivery kind.
  def retrieve_delivery_kind_by_delivery_method(delivery_method_id)
    path = "/deliveryKinds?deliveryMethodId=#{delivery_method_id}"
    response = perform_get_request(path)

    JSON.parse(response.body).first
  end

  # Retrieves the delivery method with the specified code.
  #
  # @param delivery_method_code [String] Delivery method code of the delivery kind.
  def retrieve_delivery_method_by_code(delivery_method_code)
    path = "/deliveryMethods?code=#{delivery_method_code}"
    response = perform_get_request(path)

    JSON.parse(response.body).first
  end

  private

  # Performs a GET request to the specified path.
  #
  # @param path [String] Path endpoint.
  def perform_get_request(path)
    Faraday.get(
      "#{base_uri}#{path}",
      nil,
      'Accept' => 'application/json'
    )
  end

  # Performs a POST request to the specified path.
  #
  # @param path [String] Path endpoint.
  # @param body [Hash] Optional request body.
  def perform_post_request(path, body = nil)
    Faraday.post(
      "#{base_uri}#{path}",
      body,
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    )
  end

  # Performs a PATCH request to the specified path.
  #
  # @param path [String] Path endpoint.
  # @param body [Hash] Optional request body.
  def perform_patch_request(path, body = nil)
    Faraday.patch(
      "#{base_uri}#{path}",
      body,
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    )
  end
end
