# frozen_string_literal: true

# An error indicating that an adapter does not point to a valid handler class descending from
#   `CourierHandler::BaseHandler`.
class AdapterUnknownError < StandardError; end
