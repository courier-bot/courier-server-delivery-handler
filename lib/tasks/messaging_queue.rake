# frozen_string_literal: true

namespace :messaging_queue do
  desc 'Listens for new messages to deliver'
  task deliveries: :environment do
    require 'nats/client'

    # TODO: Is this the best way to make rake tasks log?
    $stdout.sync = true
    Rails.logger = Logger.new(STDOUT)
    Rails.logger.level = Logger::INFO

    host = ENV['MESSAGING_QUEUE_HOST']
    port = ENV['MESSAGING_QUEUE_PORT']
    user = ENV['MESSAGING_QUEUE_USER']
    password = ENV['MESSAGING_QUEUE_PASSWORD']

    delivery_service = DeliveryService.new

    opts = {
      servers: ["nats://#{user}:#{password}@#{host}:#{port}"]
    }

    NATS.start(opts) do |nc|
      nc.subscribe('deliveries', queue: 'job.workers') do |delivery_msg|
        Rails.logger.info "Received request for delivery `#{delivery_msg}`"

        delivery_fields = JSON.parse(delivery_msg)
        delivery_service.deliver(delivery_fields)
      end
    end
  end
end
