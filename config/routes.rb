# frozen_string_literal: true

Rails.application.routes.draw do
  resources :messages, only: %i[] do
    resources :deliveries, only: %i[create]
  end
end
