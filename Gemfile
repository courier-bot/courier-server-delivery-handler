# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.6.3'

# Base courier handler.
gem 'courier_handlers',
    git: 'https://gitlab.com/courier_bot/courier_handlers/base_handler.git'

# Base courier handler for emails.
gem 'courier_handlers-email_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/email_handler.git'

# Courier handler for the SendGrid third-party service.
gem 'courier_handlers-sendgrid_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/sendgrid_handler.git'

# Base courier handler for text messages.
gem 'courier_handlers-text_message_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/text_message_handler.git'

# Courier handler for the Twilio third-party service.
gem 'courier_handlers-twilio_handler',
    git: 'https://gitlab.com/courier_bot/courier_handlers/twilio_handler.git'

# Allows HTTP requests.
gem 'faraday'

# NATS messaging queue.
gem 'nats'

# Application server.
gem 'puma'

# Server-side web application framework.
gem 'rails', '~> 5.2.3'

# sprockets 4.0.0 has a path traversal vulnerability (CVE-2018-3760).
# Upgrade to sprockets 4.0.0.beta8 or newer
gem 'sprockets', '4.0.0.beta8'

group :test do
  # Generates fake values.
  gem 'faker'

  # Allows for mutation testing. This gem is handled by bundler because it needs to know about
  # `rspec` and all other gem versions.
  gem 'mutant-rspec', require: false

  # Allows for general testing. All tests are in the `spec/` folder.
  gem 'rspec-rails'

  # Prevents all HTTP requests from leaving the tests.
  gem 'webmock'
end
