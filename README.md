# README

The Delivery Handler is in charge of delivering messages. It will perform requests to the client,
the Template Server, and the third-party service.

## DEVELOPMENT

NATS information can be displayed by opening a browser to `http://localhost:8222`.

Furthermore, a message can be manually pushed to the messaging queue:
```
docker-compose exec delivery-handler bundle exec nats-pub deliveries -s nats://nats_client:password@messaging-queue:4222 '{"messageId":"00000000-0000-0000-0000-000000000000"}'
```
